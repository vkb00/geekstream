﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GeekStream.Core.Shared.Entities
{
    public class Author
    {
        public int Id { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }

        public Author(int id, string fname, string lname)
        {
            
            if (string.IsNullOrEmpty(fname))
            {
                throw new ArgumentException(nameof(fname));
            }
            if (string.IsNullOrEmpty(lname))
            {
                throw new ArgumentException(nameof(lname));
            }

            Id = id;
            FName = fname;
            LName = lname;
        }
    }
}
