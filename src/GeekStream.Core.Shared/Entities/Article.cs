﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GeekStream.Core.Shared.Entities
{
    public class Article
    {
        public string Title { get; set; }
        public string Content { get; set; }
        public Author Author { get; set; }

        public Article(string title, string content, Author author)
        {
            if (string.IsNullOrWhiteSpace(title))
            {
                throw new ArgumentException(nameof(title));
            }

            if (string.IsNullOrEmpty(content))
            {
                throw new ArgumentException(nameof(content));
            }
            
            Title = title;
            Content = content;
            Author = author;
        }

    }
}
