﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GeekStream.Core.Shared.Entities;

namespace GeekStream.Core.Shared.Abstractions.DataAccess
{
    public interface IAuthorRepository
    {
        IEnumerable<Author> GetAuthors();
    }
}
