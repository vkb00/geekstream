﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GeekStream.Core.Shared.Abstractions.DataAccess;
using GeekStream.Core.Shared.Entities;


namespace GeekStream.Core.Services
{
    public class ArticlesService
    {
        private readonly IArticleRepository _articleRepository;

        public ArticlesService(IArticleRepository articleRepository)
        {
            _articleRepository = articleRepository;
        }

        public IEnumerable<Article> GetArticles()
        {
            return _articleRepository.GetArticles();
        }
    }


}
}
