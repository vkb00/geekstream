﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GeekStream.Core.Shared.Abstractions.DataAccess;
using GeekStream.Core.Shared.Entities;

namespace GeekStream.Core.Services
{
    public class AuthorService
    {
        private readonly IAuthorRepository _authorRepository;

        public AuthorService(IAuthorRepository authorRepository)
        {
            _authorRepository = authorRepository;
        }

        public IEnumerable<Author> GetAuthors()
        {
            return _authorRepository.GetAuthors();
        }
    }
}
