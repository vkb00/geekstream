﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GeekStream.Core.Shared.Abstractions.DataAccess;
using GeekStream.Core.Shared.Entities;


namespace GeekStream.Infrastructure.DataAccess
{
    public class ArticleRepository: IArticleRepository
    {
        public IEnumerable<Article> GetArticles()
        {
            return new List<Article>
            {
                new Article("Статья 1", "Контент статьи 1", new Author(1, "fname 1", "lname 1")),
                new Article("Статья 2", "Контент статьи 2", new Author(2, "fname 2", "lname 2"))
            };
        }

    }
}
