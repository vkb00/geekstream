﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GeekStream.Core.Shared.Abstractions.DataAccess;
using GeekStream.Core.Shared.Entities;

namespace GeekStream.Infrastructure.DataAccess
{
    public class AuthorRepository : IAuthorRepository
    {
        public IEnumerable<Author> GetAuthors()
        {
            return new List<Author>
            {
                new Author(1, "fname 1", "lname 1"),
                new Author(2, "fname 2", "lname 2")
            };
        }
    }
}
